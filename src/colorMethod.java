import java.util.Scanner;

public class colorMethod {
	static Scanner inputScanner = new Scanner(System.in);
	static double fargBurk = 0;
	static double meter1 = 0;
	static double meter2 = 0;
	static double door1 = 0;
	static double door2 = 0;
	static String tempString = "";
	static double paintAmount;
	
	public static void main(String[] args) {
		
		while(!tempString.contains("nej")){ // K�r s� l�nge du inte skriver 'nej'
			
			System.out.println("Hur bred �r v�ggen?(m)"); // Fr�ga
			meter1 += inputScanner.nextDouble(); // User input
			System.out.println("Hur h�g �r v�ggen?(m)"); // Fr�ga
			meter2 += inputScanner.nextDouble(); // User input
			
			System.out.println("En till v�gg?"); // Fr�ga
			tempString = inputScanner.next(); // User input
			
		}
		
		
		System.out.println("Hur stor �r f�rgburken?(m�)"); // Fr�ga
		fargBurk = inputScanner.nextDouble(); // User input
		
		System.out.println("L�gg till en d�rr?"); // Fr�ga
		if(inputScanner.next().contains("ja")){ // User input - k�r om du trycker ja
			
			System.out.println("Hur bred �r d�rren?(m)"); // Fr�ga
			door1 = inputScanner.nextDouble(); // User input
			System.out.println("Hur h�g �r d�rren?(m)"); // Fr�ga
			door2 = inputScanner.nextDouble(); // User input
			
			paintAmount = getPaintLDoor(meter1, meter2, door1, door2, fargBurk); // G� till metoden getPaintLDoor
			
		}
		else {
			
			paintAmount = getPaintL(meter1, meter2, fargBurk); // G� till metoden getPaintL
			
		}
		
		System.out.println("Du beh�ver: " + paintAmount + "L f�rg"); // Skriv ut hur mycket f�rg du beh�ver i liter
		
		System.out.println("Antal burkar: " + getBurk(paintAmount)); // G� till metoden getBurk, skriv sedan ut antal burkar
		
	}
	public static double getPaintL(double d1, double d2, double fb){
		
		System.out.println("Arean �r: " + (d1 * d2) + "m�"); // R�knar och skriver ut arean som ska m�las
		
		return ((d1 * d2)/fb); // R�knar antal liter f�rg som beh�vs och retunerar det
		
	}
	public static double getPaintLDoor(double f1, double f2, double d1, double d2, double fb){
		
		System.out.println("Arean �r: " + ((f1 * f2)-(d1 * d2)) + "m�"); // R�knar och skriver ut arean som ska m�las
		
		return ((f1 * f2)/fb)-((d1*d2/fb)); // R�knar antal liter f�rg som beh�vs och returnerar det
		
	}
	public static double getBurk(double d1) {
		
		return (int) Math.round(d1+0.49); // R�knar ut antalet burkar du beh�ver genom att avrunda till n�rmaste heltal och retunerar det
		
	}
	
}
