import java.io.IOException;
import java.io.PrintWriter;

import java.util.Scanner;
public class ArrayStopToFile {

	public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);
		PrintWriter out = new PrintWriter("C:/name.txt"); //Create a new PrintWriter that writes to path/file given
		
		String[] f_namn = new String[5];
		String[] e_namn = new String[5];
		String[] nummer = new String[5];
		
		for(int i = 0; i <= f_namn.length-1; i++){
			System.out.print("Name?\n");
			/*while (scanner.hasNextInt()) { //Remove comments to enable letters-only check.
	            System.out.println("Letters only!");
	            scanner.next();
			}*/
			f_namn[i] = scanner.next();
			if(f_namn[i].equals("Stop") || f_namn[i].equals("stop")){
				for(int y = i; y <= f_namn.length-1; y++){
					f_namn[y]="";
				}
				break;
			}
			System.out.print("Surname?\n");
			e_namn[i] = scanner.next();
			System.out.print("Number?\n");
			while (!scanner.hasNextInt()) {
	            System.out.println("Type a number!");
	            scanner.next();
	        }
			nummer[i] = scanner.next();
		}
		for (int i = 0; i <= f_namn.length-1; i++){
			if(f_namn[i] != "" && f_namn[i] != "Stop" && f_namn[i] != "stop"){
				out.println(f_namn[i] + " " + e_namn[i] + " - " + nummer[i]); //Print contents to file 
			}
		}
		out.close(); //Close to stop printing
	}

}
