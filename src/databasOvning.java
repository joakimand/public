import java.util.Scanner;
import java.sql.*;
public class databasOvning {
	
	static Scanner scann = new Scanner(System.in);
	static String HOST = "jdbc:mysql://localhost:3306/testdatabas";
	static String USER = "root";
	static String PASS = "";
	static String scanString;
	static String[] columnName = new String[] {"employee id", "first name", "last name", "email", "phone number", "hire date", "job id", "salary", "comission pct", "manager id", "department id"};
	static int counter = 1;
	static int metaInt = 0;
	
	public static void main(String[] args) {
		for(int end = 1; end != 6;)
		try {
			
			
			//Connection conn = DriverManager.getConnection(HOST, USER, PASS);
			//Statement stmt = conn.createStatement();
			
			System.out.println("Menu" + "\n\n1 All employees\n" + "2 First Name\n" + "3 Last Name\n" + "4 Department ID\n" + "5 Manager ID\n" + "6 Exit");
			
			//ResultSet rSet = stmt.executeQuery("select * from employees;");
			//ResultSetMetaData metaData = rSet.getMetaData();
			//metaInt = metaData.getColumnCount();			
			while(!scann.hasNextInt()){
				System.out.println("A number!");
				scann.next();
			}
			scanString = scann.nextLine();
			switch (scanString){
				case "1":
					all();
				break;
				case "2":
					firstName();
				break;
				case "3":
					last();
				break;
				case "4":
					dep();
				break;
				case "5":
					manage();
				break;
				case "6":
					end = 6;
					break;
			}
		}
		catch (Exception exc){
			exc.getStackTrace();
		}
	}
	private static void all() throws SQLException{
		Connection conn = DriverManager.getConnection(HOST, USER, PASS);
		Statement stmt = conn.createStatement();
		ResultSet rSet = stmt.executeQuery("SELECT * FROM employees;");
		ResultSetMetaData metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			System.out.println("\n" + counter);
			for(int i = 1; i <= metaInt; i++){
				System.out.println(columnName[i-1] + ":" + " " + rSet.getString(i));
			}
			counter++;
		}
		scanString = scann.next();
	}
	private static void firstName() throws SQLException{
		
		Connection conn = DriverManager.getConnection(HOST, USER, PASS);
		Statement stmt = conn.createStatement();
		ResultSet rSet = stmt.executeQuery("SELECT first_name FROM employees;");
		ResultSetMetaData metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			for(int i = 1; i <= metaInt; i++){
				System.out.println(counter + " " + rSet.getString(i));
			}
			counter++;
		}
		counter = 1;
		System.out.println("\nA-Z? Enter a letter.");
		while(scann.hasNextInt()){
			System.out.println("A-Z!");
			scann.next();
		}
		scanString = scann.nextLine();
		rSet = stmt.executeQuery("SELECT first_name, employee_id FROM employees WHERE first_name LIKE '" + scanString + "%';");
		metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			System.out.print(counter + " ");
			for(int i = 1; i <= metaInt; i++){
				System.out.print(rSet.getString(i) + " ");
			}
			System.out.print("\n");
			counter++;
		}
		System.out.println();
		System.out.println("Edit? Choose by employee ID.");
		while(!scann.hasNextInt()){
			System.out.println("Employee ID - a number!");
			scann.next();
		}
		scanString = scann.nextLine();
		rSet = stmt.executeQuery("SELECT * FROM employees WHERE employee_id LIKE '" + scanString + "';");
		String tString = scanString;
		metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			for(int i = 1; i <= metaInt; i++){
				System.out.println(columnName[i-1] + ":" + " " + rSet.getString(i));
			}
		}
		while(scanString != "1"){
			System.out.println("\nWhat do you want to edit?\nType exit to exit to main menu.");
			scanString = scann.nextLine();
			scanString = scanString.replace(" ", "_").toLowerCase();
			if(scanString.contains("exit")){
				break;
			}
			System.out.println("Insert value.");
			String aString = scann.nextLine();
			stmt.executeUpdate("UPDATE employees SET " + scanString + "= '" + aString + "'" + "WHERE employee_id=" + tString + ";");
			
			rSet = stmt.executeQuery("SELECT * FROM employees WHERE employee_id LIKE '" + tString + "';");
			while(rSet.next()){
				for(int i = 1; i <= metaInt; i++){
					System.out.println(rSet.getString(i));
				}
			}
		}
	}
	private static void last() throws SQLException{
		
		Connection conn = DriverManager.getConnection(HOST, USER, PASS);
		Statement stmt = conn.createStatement();
		ResultSet rSet = stmt.executeQuery("SELECT last_name FROM employees;");
		ResultSetMetaData metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			for(int i = 1; i <= metaInt; i++){
				System.out.println(counter + " " + rSet.getString(i));
			}
			counter++;
		}
		counter = 1;
		System.out.println("\nA-Z? Enter a letter.");
		while(scann.hasNextInt()){
			System.out.println("A-Z!");
			scann.next();
		}
		scanString = scann.nextLine();
		rSet = stmt.executeQuery("SELECT last_name, employee_id FROM employees WHERE last_name LIKE '" + scanString + "%';");
		metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			System.out.print(counter + " ");
			for(int i = 1; i <= metaInt; i++){
				System.out.print(rSet.getString(i) + " ");
			}
			System.out.print("\n");
			counter++;
		}
		System.out.println();
		System.out.println("Edit? Choose by employee ID.");
		while(!scann.hasNextInt()){
			System.out.println("Employee ID - a number!");
			scann.next();
		}
		scanString = scann.nextLine();
		rSet = stmt.executeQuery("SELECT * FROM employees WHERE employee_id LIKE '" + scanString + "';");
		String tString = scanString;
		metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			for(int i = 1; i <= metaInt; i++){
				System.out.println(columnName[i-1] + ":" + " " + rSet.getString(i));
			}
		}
		while(scanString != "1"){
			System.out.println("\nWhat do you want to edit?\nType exit to exit to main menu.");
			scanString = scann.nextLine();
			scanString = scanString.replace(" ", "_").toLowerCase();
			if(scanString.contains("exit")){
				break;
			}
			System.out.println("Insert value.");
			String aString = scann.nextLine();
			stmt.executeUpdate("UPDATE employees SET " + scanString + "= '" + aString + "'" + "WHERE employee_id=" + tString + ";");
			
			rSet = stmt.executeQuery("SELECT * FROM employees WHERE employee_id LIKE '" + tString + "';");
			while(rSet.next()){
				for(int i = 1; i <= metaInt; i++){
					System.out.println(rSet.getString(i));
				}
			}
		}
	
	}
	private static void dep() throws SQLException{
		
		Connection conn = DriverManager.getConnection(HOST, USER, PASS);
		Statement stmt = conn.createStatement();
		ResultSet rSet = stmt.executeQuery("SELECT DISTINCT department_id FROM employees ORDER BY department_id + 0;");
		ResultSetMetaData metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			for(int i = 1; i <= metaInt; i++){
				System.out.println(rSet.getString(i));
			}
			
		}
		
	}
	private static void manage() throws SQLException{
		
		Connection conn = DriverManager.getConnection(HOST, USER, PASS);
		Statement stmt = conn.createStatement();
		ResultSet rSet = stmt.executeQuery("SELECT DISTINCT manager_id FROM employees ORDER BY manager_id + 0;");
		ResultSetMetaData metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			for(int i = 1; i <= metaInt; i++){
				System.out.print(rSet.getString(i) + " ");
			}
			System.out.println();
			
		}
		System.out.println("Choose an ID");
		scanString = scann.next();
		rSet = stmt.executeQuery("SELECT first_name, last_name FROM employees WHERE manager_id LIKE '" + scanString + "';");
		metaData = rSet.getMetaData();
		metaInt = metaData.getColumnCount();
		while(rSet.next()){
			for(int i = 1; i <= metaInt; i++){
				System.out.print(rSet.getString(i) + " ");
			}
			System.out.println();
		}
		scann.next();
	}
}
