package persons;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBConnect {

	protected Connection myConn;
	protected Statement myStmt;
	protected ResultSet myRs;
	protected String url;
	protected String database;
	protected String table;
	protected String user;
	protected String password;
	//String StringCon = "DriverManager.getConnection("+"jdbc:mysql://localhost:3306/people"+", "+"root"+","+ ""+");";
	
	//-------------------------------
		//If you own this specific host
	DBConnect(String database, String table){
		this.url = "jdbc:mysql://localhost:3306/";
		this.database = database;
		this.table = table;
		this.user = "root";
		this.password = "";
		try {
			myConn = DriverManager.getConnection(url + database, user, password);
			myStmt = myConn.createStatement();
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	//-------------------------------
		//If you don't know any of the information
	DBConnect(String url, String database, String table, String user, String password){
		this.url = url;
		this.database = database;
		this.table = table;
		this.user = user;
		this.password = password;
		try {
			myConn = DriverManager.getConnection(url + database, user, password);
			myStmt = myConn.createStatement();
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
}
