package persons;

import java.sql.SQLException;

public class ToDB extends DBConnect {
		
	private String a, b, c, d;
	private int nr = 2; //Change 2 to any or remove for auto_increment
	
	ToDB(String database, String table){
		super(database,table);
		
	}
	ToDB(String url, String database, String table, String user, String password){
		super(url,database,table,user,password);
		// skicka in arrayList och tilldela den till variabler
		
	}
	
	//-----------------------------------------
	
	public void saveToDatabase(String a){
		this.a = a;
		this.b = "UNKNOWN";
		this.c = "UNKNOWN";
		this.d = "UNKNOWN";
		try {
			myStmt.executeUpdate(insert());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//-----------------------------------------
	
	public void saveToDatabase(String a, String b){
		this.a = a;
		this.b = b;
		this.c = "UNKNOWN";
		this.d = "UNKNOWN";
		try {
			myStmt.executeUpdate(insert());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//-----------------------------------------
	
	public void saveToDatabase(String a, String b, String c){
		this.a = a;
		this.b = b;
		this.c = b;
		this.d = "UNKNOWN";
		try {
			myStmt.executeUpdate(insert());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//-----------------------------------------
	
	public void saveToDatabase(String a, String b, String c, String d){
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		try {
			myStmt.executeUpdate(insert());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private String insert(){
		String insert = "INSERT INTO " + database + "." + table + 
				" VALUES('"+a+"','"+b+"','"+c+"','"+d+"',"+nr+");"; //Change nr to any or remove for auto_increment
		return insert;
	}
}
