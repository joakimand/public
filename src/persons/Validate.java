package persons;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {
	private static Pattern p;
	private static Matcher m;
	
	public boolean valFname(String Name){
		p = Pattern.compile("");
		m = p.matcher(Name);
		return m.matches();
	}
	public boolean valEmail(String email){
		p = Pattern.compile("^[a-zA-Z]{1}[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.[a-z]{2,4}$");
		m = p.matcher(email);
		return m.matches();
	}
	public boolean valPhone(String phoneNr){
		p = Pattern.compile("^[0]{1}[0-9]{1,4}[._-]?[0-9]{6,7}$");
		m = p.matcher(phoneNr);
		return m.matches();
	}
	
}
