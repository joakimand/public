package persons;
import java.sql.SQLException;
import java.util.ArrayList;
public class FromDB extends DBConnect {
	
	private ArrayList<Person> per;
	
	FromDB(String database, String table){
		super(database,table);
		
	}
	FromDB(String url, String database, String table, String user, String password){
		super(url,database,table,user,password);
		
	}
	public ArrayList<Person> toPerson(){
		try{
			myRs = myStmt.executeQuery("SELECT * from "+database+"."+table+";");
			per = new ArrayList<Person>();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return  per;
	}
}
