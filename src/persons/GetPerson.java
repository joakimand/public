package persons;
import java.util.ArrayList;
import java.util.Scanner;
public class GetPerson {
	
	private static Scanner inputScanner = new Scanner(System.in); // create new Scanner instance
	private ArrayList<Person> per = new ArrayList<Person>(); // new Person object/instance
	
	GetPerson(ArrayList<Person> per){
		this.per = per;
	}
	
	
	
	public void fetchPerson(int i){
		System.out.println("Fetch a person.\nPress: \n1 for Name.\n2 for Surame.\n3 for Email.\n4 for Phone number.");
		switch(inputScanner.nextInt()){
			case 1: fetchFname(i);
				break;
			case 2: fetchSname(i);
				break;
			case 3: fetchEmail(i);
				break;
			case 4: fetchPhoneNr(i);
				break;
		}
	}
	void updateList(ArrayList<Person> per){
		this.per = per;
	}
	void fetchFname(int i){
		System.out.println(per.get(i).getFname());
	}
	void fetchSname(int i){
		System.out.println(per.get(i).getSname());
	}
	void fetchEmail(int i){
		System.out.println(per.get(i).getEmail());
	}
	void fetchPhoneNr(int i){
		System.out.println(per.get(i).getPhoneNr());
	}
}
