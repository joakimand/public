package persons;
import java.util.ArrayList; // import ArrayList class
import java.util.Scanner; // import Scanner class

public class WritePerson {
	
	private static Scanner inputScanner = new Scanner(System.in); // create new Scanner object/instance
	private static Validate val = new Validate(); // new Validate object/instance
	private ArrayList<Person> per = new ArrayList<Person>(); // new Person object/instance
	
	String tempString, tempName, tempSurname, tempEmail, tempNr, tempGender;
	
	// constructor
	WritePerson(){
		writeFname();
		writeSname();
		writeEmail();
		writePhoneNr();
		
		per.add(new Person(tempName, tempSurname, tempEmail, tempNr)); // ArrayList	
		
	}
	
	//---------------------------
		//methods
	public ArrayList<Person> getList(){
		return per;
	}
	private void writeFname(){
		System.out.println("Input firstname:");
		tempName = inputScanner.next();
	}
	private void writeSname(){
		System.out.println("Input surname:");
		tempSurname = inputScanner.next();
	}
	private void writeEmail(){
		System.out.println("Input email:"); // print message
		tempEmail = inputScanner.next(); // new value from input
		for(int i = 0; i == 0;){ // never ending loop

			if(val.valEmail(tempEmail)==true){ // validateEmail, if true, break
				break;
			}
			else
				System.out.println("Invalid email!"); // print message
				tempEmail = inputScanner.next(); // new value from input
		}
	}
	private void writePhoneNr(){
		System.out.println("Input phone number:"); // print message
		tempNr = inputScanner.next(); // new value from input
		for(int t = 0; t == 0;){ // never ending loop

			if(val.valPhone(tempNr)==true){ // validateEmail, if true, break
				break;
			}
			else
				System.out.println("Invalid phone number! "); // print message
				tempNr = inputScanner.next(); // new value from input
		}
	}
}
