package persons;

import java.util.ArrayList;
import java.util.Scanner;

public class PersName {

	static WritePerson writePerson; //new Person object called writePerson
	static GetPerson getPerson; //new GetPerson object called getPerson
	static Scanner inputScanner = new Scanner(System.in);
	static ArrayList<Person> per = new ArrayList<Person>(); // new ArrayList with the data type <Person>
	static String database = "people"; //String database containing the database name "people"
	static String table = "person"; //String table containing the table name "person"
	static ToDB saveToDatabase = new ToDB(database,table); //new ToDB object from instance using a database name and table name
	static FromDB fetchFromDatabase = new FromDB(database,table);//new FromDB object from instance using a database name and table name
	static int slot = 0; //int for usage with ArrayList .get(x) method
	
	public static void main(String[] args) { // main(String[] args) start point
		
		writePerson = new WritePerson(); //write new person data
		per = writePerson.getList(); //add it to local ArrayList called "per"
		
		//---------------------
			//using the ArrayList per to fetch persons or info about them
		getPerson = new GetPerson(per); //create a new instance of getPerson using the person(s) we created 
		System.out.println("Input slot number:"); //print to screen
		slot = inputScanner.nextInt(); //new value from user input
		getPerson.fetchPerson(slot); //call the getPerson method fetchPerson() which prints a person to the screen
		
		//---------------------
			//write to and read from a database instead
		for(int i = 0; i < per.size(); i++){ // write to
			saveToDatabase.saveToDatabase(per.get(i).getFname()); // will get person i's variable(s) and send it to the class ToDB
		}
		//per = fetchFromDatabase.toPerson();
	}
}
