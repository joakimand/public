package persons;

public class Person {
	protected String fName; // new class-global variables
	protected String sName; 	// public = anyone can access (access = see and use)
	protected String email;		// private = only that class can access
	protected String phoneNr;	// protected = class, package and subclass can access
								// nothing = class and package can access
	
	// constructors
	Person(String fName){
		this.fName = fName;
		this.sName = "UNKNOWN";
		this.email = "no_email";
		this.phoneNr = "no_phone_number";
	}
	Person(String fName, String sName){
		this.fName = fName;
		this.sName = sName;
		this.email = "no_email";
		this.phoneNr = "no_phone_number";
	}
	Person(String fName, String sName, String email){
		this.fName = fName;
		this.sName = sName;
		this.email = email;
		this.phoneNr = "no_phone_number";
	}
	Person(String fName, String sName, String email, String phoneNr){
		this.fName = fName;
		this.sName = sName;
		this.email = email;
		this.phoneNr = phoneNr;
	}
	
	//------------------------
		//methods
	String getPerson(){ 
		String person = fName + "\n" + sName + "\n" + email + "\n" + phoneNr;
		return person;
	}
	String getFname(){
		return this.fName;
	}
	String getSname(){
		return this.sName;
	}
	String getEmail(){
		return this.email;
	}
	String getPhoneNr(){
		return this.phoneNr;
	}	
}
