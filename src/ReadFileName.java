import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ReadFileName {

	public static void main(String[] args) throws IOException {
		
		File input = new File("C:/name.txt"); // New File class from file
		Scanner readF = new Scanner(input); //New Scanner class that "scans" the File
		
		String name; //New String
		
		while(readF.hasNextLine()){ //Run as long as there are something in the file
			name = readF.nextLine(); //Give the String the scanned contents to be able to print to console
			System.out.println(name); //Print
		}

	}

}
