import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class LottoIn {

	public static void main(String[] args) throws IOException {
		
		File file = new File("Lotto/Kupong.txt");
		Random rand = new Random();
		Scanner scanner = new Scanner(file);
		
		int count2 = 0;
		int count = 0;
		int[] nummer = new int[7];
		int[] tillnr = new int[4];
		int[] kupoStorage = new int[7];
		
		for(int i = 0; i < 7; i++){ //H�mta fr�n fil alla rader och stoppa in dem i en array
			kupoStorage[i] = scanner.nextInt();// -.-
			System.out.print(kupoStorage[i] + " "); //Skriv ut den
		}
		System.out.print("<-- Din rad.\n"); //+ lite text.
		for(int i = 0; i < 7; i++){ // S� l�nge i <= 7 ge nummer[i] ett nummer, 1-35
			nummer[i] = rand.nextInt(35)+1;
			for(int x = 0; x <= i-1; x++){ 
				if(nummer[i] == nummer[x]){
					i--;
				}
			}
		}
		Arrays.sort(nummer);
		for(int i = 0; i < 7; i++){
			System.out.print(nummer[i]+ " ");
		}
		System.out.print("+ "); // +
		for(int i = 0; i < 4; i++){ // Till�ggsnummer
			tillnr[i] = rand.nextInt(35)+1;
			for(int x = 0; x <= i-1; x++){
				if(tillnr[i] == tillnr[x]){
					i--;
				}
			}
			System.out.print(tillnr[i] + " ");
		}
		System.out.print("<-- Lottade nummer.\n");
		for(int k = 1; k <= 35; k++){ //Kolla 35ggr
			for(int i = 0; i < 7; i++){ //7ggr varje g�ng //En till for sats med 7 ggr som l�ggs p� storage
				for(int y = 0; y < 7; y++){
					if(nummer[i] == k && kupoStorage[y] == k){ //Om nummer[i] och din rad har det nummret
						System.out.print(nummer[i] + " "); //skriv ut det
						count++; //+1 p� antal r�tt
					}
				}
			}
			for(int i = 0; i < 4; i++){ //4ggr varje g�ng //En till for som kollar 7ggr
				for(int y = 0; y < 7; y++){
					if(tillnr[i] == k && kupoStorage[y] == k){ //Om till�ggsnummret �r lika som rad 
						System.out.print(tillnr[i] + " "); // Skriv det
						count2++; //+1 i tilll�ggsraden
					}
				}
			}
		}
		if(count > 0 || count2 > 0) // S� l�nge ingen av dem blir 0 po�ng
			System.out.print(count + " + " + count2); // Skriv resultat!
		else
			System.out.print("Sorry, nothing!");
	}

}
