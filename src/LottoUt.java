//import
import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;

public class LottoUt {

	public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);
		File dir = new File("Lotto"); // Ny dir class
		dir.mkdirs(); // Skapa mapp om ite finns
		PrintWriter out = new PrintWriter("Lotto/kupong.txt"); //Ny textfil i mapp
		
		int[] nr = new int[7]; // Starts at 1 instead of 0
		
		System.out.print("Give me 7 numbers, 1-35!"); // Fr�ga efter 7 nummer
		for(int i = 0; i < 7; i++ ){ //7ggr
			if(i > 1){ // Ville inte ha texten f�rsta g�ngen
				System.out.print("Another one, 1-35!");
			}
			while(!scanner.hasNextInt()){ // Skrev du ett tecken?
				System.out.print("A number, 1-35!"); 
				scanner.next(); // Skriv igen!
			}
			nr[i] = scanner.nextInt(); //Tilldela array [nummerpos] det du skrev
			while(nr[i] > 35 || nr[i] < 1){ // �r det du skrev st�rre �n 35 eller mindre �n 0?
				System.out.print("1-35!");
				nr[i] = scanner.nextInt(); // Skriv igen!
			}
			System.out.println(nr[i]); // Skriv till sk�rm de nummer du skrivit
		}
		for(int i = 0; i < 7; i++ ){ // K�r 7ggr
			out.println(nr[i]); //F�r att tilldela varje fack till filen
		}
		out.close(); // St�ng filhantering
	}

}
